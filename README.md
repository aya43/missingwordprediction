# Missing Word Prediction (Using Nearest Neighbour)

+ Input: matrix of articles with random words deleted and collaboration information
+ Output: Predicts what words were most likely deleted from each article

Please refer to the sections inside [nearest_neighbour.R](./nearest_neighbour.R) for more information. The code is separated into sections but can be ran all at once as a complete pipeline.